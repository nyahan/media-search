package com.mediasearch.api.service;

import com.mediasearch.api.domain.OmdbMovie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class OmdbServiceImpl implements ApiService {

    @Autowired
    private OmdbClientImpl omdbClient;

    @Override
    public List<OmdbMovie> search(String query) {
        return Arrays.asList(omdbClient.search(query));
    }
}
