package com.mediasearch.api.service;


import com.mediasearch.api.domain.OmdbMovie;

public interface ApiClient {

    OmdbMovie[] search(String query);
}