package com.mediasearch.api.service;

import com.mediasearch.api.domain.OmdbMovie;
import com.mediasearch.api.domain.OmdbSearchResult;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OmdbClientImpl implements ApiClient{

    private static String OMDB_API_SEARCH_URL = "http://www.omdbapi.com/?s=%s&type=movie&y&apikey=835e94b5";

    private RestTemplate restTemplate;

    public OmdbClientImpl(RestTemplateBuilder restTemplateBuilder){
        restTemplate =restTemplateBuilder.build();
    }

    public OmdbMovie[] search(String query) {

        String uri = String.format(OMDB_API_SEARCH_URL, query);
        OmdbSearchResult result = restTemplate.getForObject(uri, OmdbSearchResult.class);
        return result.getMovies();
    }

}