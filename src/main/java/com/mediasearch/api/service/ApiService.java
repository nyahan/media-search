package com.mediasearch.api.service;

import com.mediasearch.api.domain.OmdbMovie;

import java.util.List;

public interface ApiService {

    List<OmdbMovie> search(String query);
}
