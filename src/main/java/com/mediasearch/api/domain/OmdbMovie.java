package com.mediasearch.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OmdbMovie {

    @JsonProperty(value="imdbID")
    private String imdbId;

    @JsonProperty(value="Title")
    private String title;

    @JsonProperty(value="Year")
    private String year;

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }


    @Override
    public String toString() {
        return "Movie: imdbId=" + imdbId + ", title=" + title + ", year=" + year ;
    }
}
