package com.mediasearch.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OmdbSearchResult {

    @JsonProperty(value="Search")
    private OmdbMovie[] movies;

    public OmdbMovie[] getMovies() {
        return movies;
    }

    public void setMovies(OmdbMovie[] movies) {
        this.movies = movies;
    }

}
