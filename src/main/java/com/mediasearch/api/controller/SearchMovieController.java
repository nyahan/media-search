package com.mediasearch.api.controller;


import com.mediasearch.api.domain.OmdbMovie;
import com.mediasearch.api.service.ApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "/movie", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public class SearchMovieController {

    @Autowired
    private ApiService apiService;

    @GetMapping(value = "/search")
    public ResponseEntity<List<OmdbMovie>> getCustomerOrders(@RequestParam String query) {
        List<OmdbMovie> movies = apiService.search(query);
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

}
