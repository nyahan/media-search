# media-search
This is a springboot application that perform a movie search on omdb.com using their REST api
and expose in return an endpoint that take a search query and perform the search.

To build:<br/>
./gradlew build

To launch the application:<br/>
java -jar ./build/libs/media-search-1.0.jar

To test:<br/>
http://localhost:8080/movie/search?query=titanic


